.PHONY: all clean

CC  := gcc
CXX := g++
RM  := rm
SED := sed

CPPFLAGS :=
CFLAGS   := -O3 -Wall
CXXFLAGS := -O3 -Wall
LDFLAGS  :=
LDLIBS   :=

OUT  := csv2carr
SRC  := $(wildcard *.c *.cpp)
BASE := $(basename $(SRC))
OBJ  := $(addsuffix .o,$(BASE))
DEP  := $(addsuffix .d,$(BASE))

all: $(OUT)

ifneq ($(MAKECMDGOALS),clean)
include $(DEP)
endif

$(OUT): $(OBJ)
	$(CXX) -o $@ $^ $(LDFLAGS) $(LDLIBS)

%.o: %.cpp
	$(CXX) -o $@ $< -c $(CPPFLAGS) $(CXXFLAGS)

%.o: %.c
	$(CC) -o $@ $< -c $(CPPFLAGS) $(CFLAGS)

%.d: %.cpp
	$(CXX) -M $(CPPFLAGS) $(CXXFLAGS) $< | $(SED) 's,\($*\)\.o[ :]*,\1.o $@ : ,g' > $@

%.d: %.c
	$(CC) -M $(CPPFLAGS) $(CFLAGS) $< | $(SED) 's,\($*\)\.o[ :]*,\1.o $@ : ,g' > $@

clean:
	$(RM) -f $(OUT) $(OBJ) $(DEP)
