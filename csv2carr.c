#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
	char c;
	const char *elem_type = "int";
	const char *brackets = "[]";
	int skip = 0;
	char **argp = argv + 1;
	int array_line_found = 0;
	while (*argp) {
		if (!strcmp(*argp, "-h") || !strcmp(*argp, "--help")) {
			printf("Usage: %s [-t|--type TYPE] [-b|--brackets BRACKETS] [-s|--skip LINES] < table.csv > arrdef.c\n", argv[0]);
			exit(EXIT_SUCCESS);
		}
		else if (!strcmp(*argp, "-t") || !strcmp(*argp, "--type")) {
			if (*(++argp) == NULL) {
				fputs("Type expected!\n", stderr);
				exit(EXIT_FAILURE);
			}
			elem_type = *argp;
		}
		else if (!strcmp(*argp, "-b") || !strcmp(*argp, "--brackets")) {
			if (*(++argp) == NULL) {
				fputs("Brackets expected!\n", stderr);
				exit(EXIT_FAILURE);
			}
			brackets = *argp;
		}
		else if (!strcmp(*argp, "-s") || !strcmp(*argp, "--skip")) {
			if (*(++argp) == NULL) {
				fputs("Number of lines to skip expected!\n", stderr);
				exit(EXIT_FAILURE);
			}
			int s = sscanf(*argp, "%d", &skip);
			if (!s || skip < 0) {
				fputs("Number of lines to skip should be a non-negative integer!\n", stderr);
				exit(EXIT_FAILURE);
			}
		}
		else {
			fprintf(stderr, "Unknown option \'%s\'!\n", *argp);
			exit(EXIT_FAILURE);
		}
		argp++;
	}
	while (skip) {
		c = getchar();
		if (c == EOF) exit (EXIT_SUCCESS);
		if (c == '\n') skip--;
	}
	while (1) {
		array_line_found = 0;
		do {
			do {
				c = getchar();
				if (c == EOF) exit (EXIT_SUCCESS);
			}
			while (c == '\n');
			if (c == ',') {
				while ((c = getchar()) != EOF && c != '\n');
				if (c == EOF) exit (EXIT_SUCCESS);
			}
			else {
				array_line_found = 1;
			}
		}
		while (!array_line_found);
		printf("%s ", elem_type);
		do {
			putchar(c);
			c = getchar();
		}
		while (c != EOF && c != ',' && c != '\n');
		printf("%s", brackets);
		if (c == ',') {
			printf(" = {");
			c = getchar();
			while (c != EOF && c != '\n') {
				putchar(c);
				c = getchar();
			}
			puts("};");
		}
		else {
			puts(";");
		}
		if (c == EOF) exit (EXIT_SUCCESS);
	}
	return EXIT_SUCCESS;
}
